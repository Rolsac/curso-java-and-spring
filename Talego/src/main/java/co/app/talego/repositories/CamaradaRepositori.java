package co.app.talego.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.app.talego.models.CamaradasModel;


@Repository
public interface CamaradaRepositori extends CrudRepository<CamaradasModel, Long> {

}
