/*
 * package co.app.talego.models;
 * 
 * import java.time.LocalDateTime;
 * 
 * 
 * import javax.persistence.*;
 * 
 * 
 * 
 * import lombok.Data;
 * 
 * @Data
 * 
 * @Entity
 * 
 * @Table(name = "producto") public class ProductoModel {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY)
 * 
 * private Long K_producto;
 * 
 * @ManyToMany(cascade = {CascadeType.ALL})
 * 
 * @JoinTable( name = "relacional", joinColumns = { @JoinColumn(name =
 * "k_producto") }, inverseJoinColumns = { @JoinColumn(name = "k_categoria")})
 * 
 * 
 * @Column(nullable = false) private String n_nombre;
 * 
 * @Column(nullable = false) private String n_url;
 * 
 * @Column(nullable = false) private String n_descripcion;
 * 
 * @Column(nullable = false) private String n_descripcion_corta;
 * 
 * @Column(nullable = false) private String n_sku;
 * 
 * @Column(nullable = false) private String i_estado;
 * 
 * @Column(nullable = false) private String i_venta_individual;
 * 
 * @Column(nullable = false) private int v_precio;
 * 
 * @Column(nullable = false) private LocalDateTime f_creacion; private
 * LocalDateTime f_update;
 * 
 * 
 * 
 * 
 * }
 */
