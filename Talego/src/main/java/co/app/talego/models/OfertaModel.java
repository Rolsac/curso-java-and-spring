/*
 * package co.app.talego.models;
 * 
 * import java.time.LocalDateTime;
 * 
 * import javax.persistence.*;
 * 
 * import lombok.Data;
 * 
 * @Data
 * 
 * @Entity
 * 
 * @Table(name = "oferta") public class OfertaModel {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long k_oferta;
 * 
 * private Long k_producto; private int v_precio; private LocalDateTime
 * f_incial; private LocalDateTime f_final; private LocalDateTime f_creacion;
 * private LocalDateTime f_update;
 * 
 * }
 */
