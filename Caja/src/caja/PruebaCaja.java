package caja;

public class PruebaCaja {
	public static void main(String args[]) {
		
		
		int medidaAncho = 4;
		int medidaAlto = 5;
		int medidaProfun = 8;
		
		Caja caja1 = new Caja();
		caja1.ancho = medidaAncho;
		caja1.alto = medidaAlto;
		caja1.profundo = medidaProfun;
		int resultado = caja1.calcularVolumen();
		
		System.out.println("El volumen de la caja 1 es: " + resultado);
		
		Caja caja2 = new Caja(2, 4, 6);
		System.out.println("El volumen de la caja 2 es: " + caja2.calcularVolumen());
	}
}
