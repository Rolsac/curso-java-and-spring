/*
 * package co.app.talego.models;
 * 
 * import java.time.LocalDateTime;
 * 
 * import javax.persistence.*;
 * 
 * import lombok.Data;
 * 
 * @Data
 * 
 * @Entity
 * 
 * @Table(name = "inventario") public class InventarioModel {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long
 * k_inventario;
 * 
 * private Long k_producto; private Long v_cantidad; private String i_estado;
 * private LocalDateTime f_creacion; private LocalDateTime f_actualizacion;
 * 
 * }
 */