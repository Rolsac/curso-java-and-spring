package co.app.talego;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalegoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TalegoApplication.class, args);
	}

}
