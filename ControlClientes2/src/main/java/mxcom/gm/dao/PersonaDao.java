package mxcom.gm.dao;

import org.springframework.data.repository.CrudRepository;

import mxcom.gm.domain.Persona;

public interface PersonaDao extends CrudRepository<Persona, Long> {
	

}
