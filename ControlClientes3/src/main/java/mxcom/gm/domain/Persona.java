package mxcom.gm.domain;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

	
	@Data
	@Entity
	@Table(name = "persona")
	public class Persona implements Serializable{
		
		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id_Persona ;
		
		@NotEmpty
		private String Nombre;
		
		@NotEmpty
		private String apellido;
		
		@NotEmpty
		@Email
		private String email;
		
		
		private String telefono ;

	}



