package co.app.talego.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.app.talego.models.CamaradasModel;
import co.app.talego.services.CamaradaService;



@RestController
@RequestMapping("/camaradas")
public class CamaradaController {
	
	@Autowired
	CamaradaService camaradaService;
	
	@GetMapping()public ArrayList<CamaradasModel> obtenrUsuarios(){
		return camaradaService.obtenerUsuarios();
	}
	
	@PostMapping()
	public CamaradasModel guardarUsuario(@RequestBody CamaradasModel usuario) {
		return this.camaradaService.guardarUsuario(usuario);		
	}
	
	@GetMapping(path = "/{id}")
	public Optional<CamaradasModel> obtenerUsuarioPorId(@PathVariable("id") Long id){
		return this.camaradaService.obtenerPorId(id);
	}
	
	
	@DeleteMapping(path = "/{id}")
	public String eliminarPorId(@PathVariable("id") Long id) {
		boolean ok = this.camaradaService.eliminarUsuario(id);
		if (ok) {
			return "El usuario con el id " + id;
		}else {
			return "No se pudo eliminar el usuario con id" + id;
		}
	}

}
