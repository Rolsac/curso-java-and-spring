/*
 * package co.app.talego.models;
 * 
 * import java.time.LocalDateTime;
 * 
 * import javax.persistence.*;
 * 
 * import lombok.Data;
 * 
 * @Data
 * 
 * @Entity
 * 
 * @Table(name = "subcategoria") public class SubCategoriaModel {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long
 * k_subcategoria;
 * 
 * private Long k_categoria_padre; private Long k_categoria_hija; private
 * LocalDateTime f_creacion; private LocalDateTime f_update; }
 */