package co.app.talego.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.app.talego.models.CamaradasModel;
import co.app.talego.repositories.CamaradaRepositori;



@Service
public class CamaradaService {
	
	@Autowired
	CamaradaRepositori camaradaRepositori;
	
	public ArrayList<CamaradasModel> obtenerUsuarios(){
		return (ArrayList<CamaradasModel>) camaradaRepositori.findAll();
	}
	
	public CamaradasModel guardarUsuario(CamaradasModel usuario) {
		return camaradaRepositori.save(usuario);
	}
	
	public Optional<CamaradasModel> obtenerPorId(Long id) {
		return camaradaRepositori.findById(id);
	}
	
	public boolean eliminarUsuario(Long id) {
		try {
			camaradaRepositori.deleteById(id);
			return true;
		} catch (Exception err) {
			return false;
		}
	}
	
	
	
	
	
	

}
