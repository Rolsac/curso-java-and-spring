package co.app.talego.models;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "camaradas")
public class CamaradasModel {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	private String nombre_completo;
	private String nombre_empresa;
	private String email;
	private String telefono;
	private String terminos;
}
