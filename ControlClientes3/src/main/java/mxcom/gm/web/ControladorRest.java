package mxcom.gm.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/talego")
public class ControladorRest {

	@GetMapping("/saludo")
	public String nombre() {
		return "Hola mundo";
	}
	
	
}
