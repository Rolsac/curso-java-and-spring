package co.com.social.models;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
public class UsuarioModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	
	private String nombre;
	private String email;
	private Integer prioridad;

}
