/*
 * package co.app.talego.models;
 * 
 * import java.time.LocalDateTime; import java.util.HashSet; import
 * java.util.Set;
 * 
 * import javax.persistence.*;
 * 
 * import lombok.Data;
 * 
 * @Data
 * 
 * @Entity
 * 
 * @Table(name = "categoria") public class CategoriaModel {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long k_categoria;
 * 
 * private Long k_imagen; private String n_nombre; private String n_url; private
 * String n_descripcion; private LocalDateTime f_creacion; private LocalDateTime
 * f_update;
 * 
 * @ManyToMany(mappedBy = "ProductoModel") private Set<ProductoModel>
 * productoModel = new HashSet<>();
 * 
 * }
 */
