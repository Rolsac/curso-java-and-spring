package mxcom.gm.domain;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

	@Data
	@Entity
	@Table(name = "persona")
	public class Persona implements Serializable{
		
		private static final long serialVersionUID = 1L;
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long telefono ;
		private String NoMBRe;
		private String apeLLido;
		private String email;
		private String id_Persona;

	}



